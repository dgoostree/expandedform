package com.goostree.codewars.expandedform;

public class Kata {
	
	public static String expandedForm(int num)
    {
		int rem;
		int i = 10;
		String result = "";
		
		do {
			rem = num % i;
			num -= rem;
			
			if(rem != 0) {
				
				if(result.length() > 0) {
					result = rem + " + " + result;
				}
				else {
					result = String.valueOf(rem);
				}
				
			}
			
			i *= 10;
		} while(num > 0);
		
		return result;
    }
	
	/* Str only solution
	public static String expandedForm(int num)
    {
		String numStr = String.valueOf(num);		
		int len = numStr.length();
		String result = "";
		
		for(int i = 0; i < len; i++) {
			if(numStr.charAt(i) != '0') {
				if(result.length() > 0) {
					result += " + ";
				}
				
				result += numStr.charAt(i);
				
				for(int j = i; j < len - 1; j++){
					result += "0";
				}
			}
		}
		
		return result;
    }*/
}
